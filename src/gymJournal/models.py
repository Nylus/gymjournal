from gymJournal import db
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from gymJournal import login

class User(UserMixin, db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

class Session(db.Model):
    __tablename__ = 'session'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, nullable=True)
    type = db.Column(db.String(64))
    program_day = db.Column(db.Integer, default=1)
    program_id = db.Column(db.Integer, db.ForeignKey('program.id'))

    program = db.relationship("Program", back_populates="sessions")
    exercises = db.relationship("Exercise", back_populates="session")

    __mapper_args__ = {
        'polymorphic_identity':'session',
        'polymorphic_on': type
    }

    def to_dict(self):
        outputDict = {"id": self.id,
                    "date": self.date.isoformat() if self.date else None,
             "program_day": self.program_day}
        exerciseList = []
        for exercise in self.exercises:
            exerciseList.append(exercise.to_dict())

        outputDict["exercises"] = exerciseList

        return outputDict

class Program(db.Model):
    __tablename__ = 'program'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    type = db.Column(db.String(64))
    name = db.Column(db.String(64), default="Test Program")
    current_day = db.Column(db.Integer, default=1)
    completeDate = db.Column(db.DateTime, nullable=True)
    sessions = db.relationship("Session", back_populates="program")

    __mapper_args__ = {
        'polymorphic_identity':'program',
        'polymorphic_on':type
    }

    icon = ""
    program_definition = []


    def to_dict(self):
        outputDict = {"id": self.id,
                "type": self.type,
                "name": self.name,
                "current_day": self.current_day,
                "complete_date": self.completeDate}

        sessionsArray = []
        for session in self.sessions:
            sessionsArray.append(session.to_dict())

        outputDict["sessions"] = sessionsArray

        return outputDict

    def is_complete(self):
        return self.completeDate is not None


    def processFormData(self, form):
        """
        processFormData(self, form)

        Process the data returned from the frontend for a specific WTForm object.
        Returns the JSON AJAX response.

        @return Dict
        """

        # Override this in subclass
        return None


    def progress(self):
        # Override this in subclass
        return "No Progress"


    def workoutForCurrentDay(self):
        workout = None
        if self.current_day <= len(self.sessions):
            workout = self.workoutForDay(self.current_day)

        return workout


    def workoutForDay(self, day):
        #workout = self.program_definition[self.current_day - 1]
        workout = Session.query.filter(Session.program_id==self.id).\
                                filter(Session.program_day==day).\
                                first()

        return workout


    #def __init__(self):
    def generatePreview(self):
        # Override this in subclass
        return ""


class Set(db.Model):
    __tablename__ = 'set'
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.String(64))
    exercise_id = db.Column(db.Integer, db.ForeignKey('exercise.id'))
    index = db.Column(db.Integer)

    exercise = db.relationship("Exercise", back_populates="sets")

    __mapper_args__ = {
        'polymorphic_identity':'set',
        'polymorphic_on':type
    }

    def to_dict(self):
        outputDict = {"id": self.id,
                    "type": self.type}

        return outputDict


from gymJournal.programs.runningSet import RunningSet

class Exercise(db.Model):
    __tablename__ = 'exercise'
    id = db.Column(db.Integer, primary_key=True)
    session_id = db.Column(db.Integer, db.ForeignKey('session.id'))
    name = db.Column(db.String(64))
    description = db.Column(db.String(256))

    sets = db.relationship("Set", back_populates="exercise")
    session = db.relationship("Session", back_populates="exercises")

    def to_dict(self):
        outputDict = {"id": self.id,
                    "name": self.name,
             "description": self.description}

        setsList = []

        for individualSet in self.sets:
            setsList.append(individualSet.to_dict())

        outputDict["sets"] = setsList

        return outputDict



#Get session user for flak-login
@login.user_loader
def load_user(id):
    return User.query.get(int(id))
