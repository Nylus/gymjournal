from gymJournal import db
from gymJournal.programs.simpleSet import SimpleSet

class WeightSet(SimpleSet):
    __tablename__ = 'weight_set'
    __mapper_args__ = {
        'polymorphic_identity':'weight_set',
    }

    weight = db.Column(db.Integer, default=0)

    def to_dict(self):
        outputDict = {"id": self.id,
                    "type": self.type,
                    "reps": self.reps,
                  "weight": self.weight,
                   "index": self.index,
                    "staticReps": self.staticReps}

        return outputDict



