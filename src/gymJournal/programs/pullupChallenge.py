from datetime import datetime
import enum

from flask import render_template, jsonify

from gymJournal import db
from gymJournal.models import Program, Session, Exercise
from gymJournal.programs.simpleSet import SimpleSet

class WeightChoices(enum.Enum):
    assisted = "assisted"
    unassisted = "unassisted"
    weighted = "weighted"

class GoalChoices(enum.Enum):
    fifty = 50
    fourty = 40
    thirty = 30
    fifteen = 15

class PullupChallenge(Program):
    __tablename__ = 'pullup_challenge'
    __mapper_args__ = {
        'polymorphic_identity':'pullup_challenge',
    }

    challengeGoal = db.Column(db.Integer)
    assistance = db.Column(db.Enum(WeightChoices), default=WeightChoices.unassisted)

    icon = "pullups.png"

    def __init__(self, user=None):
        self.name = "Pullup Challenge"
        if user:
            self.user_id = user.id


        #def distanceRunMi(distance=1):
        #    exercise = Exercise()
        #    distSet = RunningSet(miDistance=distance)
        #    exercise.sets = [distSet]
        #    exercise.name = str(distance) + "mi Run"

        #    return exercise

        #def run(dist, description=None):
        #    session = Session()
        #    session.program_id = self.id

        #    exercise = distanceRunMi(distance=dist)
        #    exercise.description = description or (str(dist) + " miles")
        #    session.exercises = [exercise]

        #    return session

        #def basic(description):
        #    session = Session()
        #    session.program_id = self.id
        #    exercise = Exercise()
        #    exercise.description = description
        #    session.exercises = [exercise]

        #    return session

        def makeProgramDefinition():
            session = Session()
            session.program_id = self.id
            exercise = Exercise()
            newSet = SimpleSet()
            newSet.index = 0
            exercise.sets = [newSet]
            exercise.name = "Pullup Challenge"

            exercise.description = ""
            session.exercises = [exercise]


            self.sessions = [session]
            #self.sessions = [
            #    run(3), basic("Crosstrain"), run(3), basic("REST"), run(3), run(3), basic("REST"),
            #    run(3), basic("Crosstrain"), run(3), basic("REST"), run(4), run(3), basic("REST"),
            #    run(3), basic("Crosstrain"), run(3), basic("REST"), run(5), run(3), basic("REST"),
            #    run(3), basic("Crosstrain"), run(3), basic("REST"), run(6), run(3), basic("REST"),
            #    run(3), basic("Crosstrain"), run(4), basic("REST"), run(4), run(3), basic("REST"),
            #    run(3), basic("Crosstrain"), run(4), basic("REST"), run(6), run(3), basic("REST"),
            #    run(3), basic("Crosstrain"), run(4), basic("REST"), run(7), run(3), basic("REST"),
            #    run(3), basic("Crosstrain"), run(4), basic("REST"), run(8), run(3), basic("REST"),
            #    run(3), basic("Crosstrain"), run(4), basic("REST"), run(5), run(3), basic("REST"),
            #    run(3), basic("Crosstrain"), run(5), basic("REST"), run(8), run(4), basic("REST"),
            #    run(3), basic("Crosstrain"), run(5), basic("REST"), run(9), run(4), basic("REST"),
            #    run(3), basic("Crosstrain"), run(5), basic("REST"), run(10), run(4), basic("REST"),
            #    run(3), basic("Crosstrain"), run(5), basic("REST"), run(7), run(4), basic("REST"),
            #    run(3), basic("Crosstrain"), run(5), basic("REST"), run(11), run(4), basic("REST"),
            #    run(3), basic("Crosstrain"), run(5), basic("REST"), run(8), run(4), basic("REST"),
            #    run(3, description="Easy 3 miles"), run(2, description="Easy 2 Miles"), run(3, description="Easy 3 miles"),
            #    basic("REST"), basic("REST"), run(13.1)
            #]

            #for index, session in enumerate(self.sessions, start=1):
            #    session.program_day = index

        makeProgramDefinition()


    def getTotalDays(self):
        return 1

    def getTotalReps(self):
        reps = 0
        for pullupSet in self.sessions[0].exercises[0].sets:
            reps += pullupSet.reps

        return reps

    def getScore(self):
        score = len(self.sessions[0].exercises[0].sets)

        return score

    def getResult(self):
        result = {'score': '',
                  'hasImproved': False}
        if self.is_complete():
            result['score'] = self.getScore()

            # Determine if current score is an improvement
            previousComparablePrograms = PullupChallenge.query.filter_by(challengeGoal=self.challengeGoal,
                                                                 assistance=self.assistance)\
                                                      .filter(Program.completeDate < self.completeDate).all()
            if len(previousComparablePrograms):
                bestScore = previousComparablePrograms[0].getScore()
                for prog in previousComparablePrograms[1:]:
                    if prog.getScore() < bestScore:
                        bestScore = prog.getScore()

                if self.getScore() < bestScore:
                    result['hasImproved'] = True


        return result

    # Override methods


    def processFormData(self, form):
        """
        processFormData(self, PullupChallengeForm)

        @return Dict: json_response
        """
        # Data is valid, save the form data to database
        
        repsList = form.pullupReps.data

        ## Save data relating to the given set to the database
        exercise = self.sessions[0].exercises[0]

        # Add new sets to hold data returned from user
        while len(repsList) > len(exercise.sets):
            # New sets need to be created
            exercise.sets.append(SimpleSet())

        # If fewer sets are returned than are in the database, delete the missing ones
        while len(repsList) < len(exercise.sets):
            db.session.delete(exercise.sets[-1])
            exercise.sets = exercise.sets[:-1]

        for x in range(len(repsList)):
            exercise.sets[x].reps = repsList[x]
            exercise.sets[x].index = x

        if form.complete.data or self.getTotalReps() >= self.challengeGoal:
            self.completeDate = datetime.now()

        db.session.add(self)
        db.session.add(exercise)
        db.session.commit()

        #return self.sessions[0].to_dict()
        return self.to_dict()

    def progress(self):
        return "Single Session"

    def to_dict(self):
        outputDict = super().to_dict()
        outputDict['result'] = self.getResult()
        outputDict['challengeGoal'] = self.challengeGoal
        outputDict['assistance'] = self.assistance.value

        return outputDict


