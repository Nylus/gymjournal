"""Add program_day to session

Revision ID: d7bed63e4e97
Revises: 846c23a9960f
Create Date: 2020-11-05 00:19:02.015494

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd7bed63e4e97'
down_revision = '846c23a9960f'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('session', sa.Column('program_day', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('session', 'program_day')
    # ### end Alembic commands ###
