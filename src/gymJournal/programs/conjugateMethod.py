from datetime import timedelta
from math import ceil

from flask import render_template, jsonify

from gymJournal import db
from gymJournal.models import Program, Session, Exercise
from gymJournal.programs.simpleSet import SimpleSet
from gymJournal.programs.weightSet import WeightSet
from gymJournal.programs.dynamicSession import DynamicSession

class ConjugateMethod(Program):
    __tablename__ = 'conjugate_method'
    __mapper_args__ = {
        'polymorphic_identity':'conjugate_method',
    }

    icon = "barbell.png"

    def __init__(self, user=None):
        self.name = "Conjugate Method"
        if user:
            self.user_id = user.id

        # Exercises
        def benchPressExercise(repMax=1, withPause=False):
            exercise = Exercise()
            withPauseText = ""
            if withPause:
                withPauseText = "w/ Pause "
            exercise.name = "Bench Press %s(Work up to %d rep Max)" % (withPauseText, repMax)
            exercise.description = "Find your bench maximum. Enter each weight you can successfully perform for three reps. As soon as you cannot bench three reps, you have found your maximum."

            initialSet = WeightSet()
            initialSet.index = 0
            initialSet.reps = repMax
            initialSet.staticReps = True
            exercise.sets = [initialSet]

            return exercise

        def floorPressExercise(sets=3, reps=8, weight=0):
            return weightExerciseWithSetsReps("Floor Press", sets, reps, True)

        def dumbbellRowsExercise(sets=4, reps=10, weight=0):
            return weightExerciseWithSetsReps("Dumbbell Rows", sets, reps, True)

        def tricepPushdownsExercise(sets=3, reps=25):
            return weightExerciseWithSetsReps("Tricep Pushdowns", sets, reps, True)

        def bicepCurlsExercise(sets=3, reps=15):
            return weightExerciseWithSetsReps("Bicep Curls", sets, reps, True)

        def weightExerciseWithSetsReps(name="", sets=1, reps=1, staticReps=False, description=""):
            return exerciseWithSetsReps(name, sets, reps, description, True)

        def exerciseWithSetsReps(name="", sets=1, reps=1, description="", isWeightExercise=False):
            exercise = Exercise()
            exercise.name = "%s (%dx%d)" % (name, sets, reps)
            exercise.description = description
            exercise.sets = []

            setType = SimpleSet
            if isWeightExercise: setType = WeightSet

            for setIndex in range(sets):
                newSet = setType()
                newSet.index = setIndex
                newSet.reps = reps

                exercise.sets.append(newSet)

            return exercise

        def boxJumpsExercise(sets=4, reps=15):
            return exerciseWithSetsReps("Box Jumps", sets, reps)

        def wideStanceBoxSquatsExercise(sets=3, reps=8):
            return exerciseWithSetsReps("Wide Stance Box Squats", sets, reps)

        def cablePullThroughsExercise(sets=3, reps=10):
            return exerciseWithSetsReps("Cable Pull Throughs", sets, reps)

        def cableRowsExercise(sets=3, reps=10):
            return exerciseWithSetsReps("Cable Rows", sets, reps)

        def basicExercise(description):
            exercise = Exercise()
            exercise.description = description

            return exercise


        # Sessions
        def maxUpperSession(weekIndex):
            session = Session()
            if weekIndex == 0:
                exercises = [benchPressExercise(repMax=3),
                             floorPressExercise(),
                             dumbbellRowsExercise(),
                             tricepPushdownsExercise(),
                             bicepCurlsExercise(),
                             basicExercise("1 Hour of Cardio")]
                session.exercises = exercises
                return session
            else:
                return None
            
            

        def maxLowerSession(weekIndex):
            session = Session()
            if weekIndex == 0:
                session.exercises = [basicExercise("1 Hour of Cardio")]
                return session
            else:
                return None

        def dynamicUpperSession(weekIndex):
            session = Session()
            if weekIndex == 0:
                session.exercises = [cableRowsExercise(),
                                     basicExercise("1 Hour of Cardio")]
                return session
            else:
                return None

        def dynamicLowerSession(weekIndex):
            session = Session()
            if weekIndex == 0:
                session.exercises = [boxJumpsExercise(),
                                     wideStanceBoxSquatsExercise(),
                                     cablePullThroughsExercise(),
                                     basicExercise("1 Hour of Cardio")]
                return session
            else:
                return None

        def basicSession(description):
            session = Session()
            session.exercises = [basicExercise(description)]

            return session

        def makeProgramDefinition():
            self.sessions = []
            for weekIndex in range(1):
                self.sessions.append(maxUpperSession(weekIndex))
                self.sessions.append(basicSession("GPP Upper"))
                self.sessions.append(dynamicLowerSession(weekIndex))
                self.sessions.append(dynamicUpperSession(weekIndex))
                self.sessions.append(basicSession("GPP Lower"))
                self.sessions.append(maxLowerSession(weekIndex))
                self.sessions.append(basicSession("REST"))

            for index, session in enumerate(self.sessions, start=1):
                session.program_day = index
                session.program_id = self.id

        makeProgramDefinition()


    def getTotalDays(self):
        return len(self.sessions)

    # Override methods


    #def processFormData(self, form):
    #    """
    #    processFormData(self, HalfMarathonForm)

    #    @return Dict: json_response
    #    """
    #    # Data is valid, save the form data to database
    #    times = form.time.data.split(":")
    #    times.reverse()
    #    seconds = int(times[0])
    #    minutes = 0
    #    hours = 0
    #    if len(times) > 1:
    #        minutes = int(times[1])
    #    if len(times) > 2:
    #        hours = int(times[2])

    #    duration = timedelta(hours=hours, minutes=minutes, seconds=seconds)
    #    # Save data relating to the given day to the database
    #    workout = self.workoutForDay(int(form.day.data))
    #    workout.exercises[0].sets[0].duration = duration
    #    self.current_day = int(form.day.data) + 1
    #    self.sessions[int(form.day.data) - 1] = workout

    #    db.session.add(self)
    #    db.session.add(workout)
    #    db.session.commit()

    #    return workout.to_dict()

    def progress(self):
        return "Day " + str(self.current_day) + " of " + str(self.getTotalDays())

    def generatePreview(self):
        DAYS = 7
        calendar_array = [self.sessions[r*DAYS:(r*DAYS)+DAYS] \
                          for r in range(ceil(len(self.sessions)/DAYS))]

        return render_template("programPreviews/conjugateMethodPreviewTable.html", definition=calendar_array)


