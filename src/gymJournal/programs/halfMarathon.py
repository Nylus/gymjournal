from datetime import timedelta
from math import ceil

from flask import render_template, jsonify

from gymJournal import db
from gymJournal.models import Program, Session, Exercise
from gymJournal.programs.runningSet import RunningSet

class HalfMarathon(Program):
    __tablename__ = 'half_marathon'
    __mapper_args__ = {
        'polymorphic_identity':'half_marathon',
    }

    icon = "running.png"

    def __init__(self, user=None):
        self.name = "Half Marathon"
        if user:
            self.user_id = user.id


        def distanceRunMi(distance=1):
            exercise = Exercise()
            distSet = RunningSet(miDistance=distance)
            exercise.sets = [distSet]
            exercise.name = str(distance) + "mi Run"

            return exercise

        def run(dist, description=None):
            session = Session()
            session.program_id = self.id

            exercise = distanceRunMi(distance=dist)
            exercise.description = description or (str(dist) + " miles")
            session.exercises = [exercise]

            return session

        def basic(description):
            session = Session()
            session.program_id = self.id
            exercise = Exercise()
            exercise.description = description
            session.exercises = [exercise]

            return session

        def makeProgramDefinition():
            self.sessions = [
                run(3), basic("Crosstrain"), run(3), basic("REST"), run(3), run(3), basic("REST"),
                run(3), basic("Crosstrain"), run(3), basic("REST"), run(4), run(3), basic("REST"),
                run(3), basic("Crosstrain"), run(3), basic("REST"), run(5), run(3), basic("REST"),
                run(3), basic("Crosstrain"), run(3), basic("REST"), run(6), run(3), basic("REST"),
                run(3), basic("Crosstrain"), run(4), basic("REST"), run(4), run(3), basic("REST"),
                run(3), basic("Crosstrain"), run(4), basic("REST"), run(6), run(3), basic("REST"),
                run(3), basic("Crosstrain"), run(4), basic("REST"), run(7), run(3), basic("REST"),
                run(3), basic("Crosstrain"), run(4), basic("REST"), run(8), run(3), basic("REST"),
                run(3), basic("Crosstrain"), run(4), basic("REST"), run(5), run(3), basic("REST"),
                run(3), basic("Crosstrain"), run(5), basic("REST"), run(8), run(4), basic("REST"),
                run(3), basic("Crosstrain"), run(5), basic("REST"), run(9), run(4), basic("REST"),
                run(3), basic("Crosstrain"), run(5), basic("REST"), run(10), run(4), basic("REST"),
                run(3), basic("Crosstrain"), run(5), basic("REST"), run(7), run(4), basic("REST"),
                run(3), basic("Crosstrain"), run(5), basic("REST"), run(11), run(4), basic("REST"),
                run(3), basic("Crosstrain"), run(5), basic("REST"), run(8), run(4), basic("REST"),
                run(3, description="Easy 3 miles"), run(2, description="Easy 2 Miles"), run(3, description="Easy 3 miles"),
                basic("REST"), basic("REST"), run(13.1)
            ]

            for index, session in enumerate(self.sessions, start=1):
                session.program_day = index

        makeProgramDefinition()


    def getTotalDays(self):
        return len(self.sessions)

    # Override methods


    def processFormData(self, form):
        """
        processFormData(self, HalfMarathonForm)

        @return Dict: json_response
        """
        # Data is valid, save the form data to database
        times = form.time.data.split(":")
        times.reverse()
        seconds = int(times[0])
        minutes = 0
        hours = 0
        if len(times) > 1:
            minutes = int(times[1])
        if len(times) > 2:
            hours = int(times[2])

        duration = timedelta(hours=hours, minutes=minutes, seconds=seconds)
        # Save data relating to the given day to the database
        workout = self.workoutForDay(int(form.day.data))
        workout.exercises[0].sets[0].duration = duration
        self.current_day = int(form.day.data) + 1
        self.sessions[int(form.day.data) - 1] = workout

        db.session.add(self)
        db.session.add(workout)
        db.session.commit()

        return workout.to_dict()

    def progress(self):
        return "Day " + str(self.current_day) + " of " + str(self.getTotalDays())

    def generatePreview(self):
        DAYS = 7
        calendar_array = [self.sessions[r*DAYS:(r*DAYS)+DAYS] \
                          for r in range(ceil(len(self.sessions)/DAYS))]

        return render_template("programPreviews/halfMarathonPreviewTable.html", definition=calendar_array)

