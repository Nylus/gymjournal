from gymJournal import db
from gymJournal.models import Set

class SimpleSet(Set):
    __tablename__ = 'simple_set'
    __mapper_args__ = {
        'polymorphic_identity':'simple_set',
    }

    reps = db.Column(db.Integer, default=0)
    staticReps = db.Column(db.Boolean, default=False)


    def to_dict(self):
        outputDict = {"id": self.id,
                    "type": self.type,
                    "reps": self.reps,
                    "index": self.index,
                    "staticReps": self.staticReps}

        return outputDict


