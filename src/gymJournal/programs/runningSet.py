from datetime import timedelta

from gymJournal import db
from gymJournal.models import Set


MILES_IN_KM = 0.621371


class RunningSet(Set):
    __tablename__ = 'running_set'
    __mapper_args__ = {
        'polymorphic_identity':'running_set',
    }

    distance = db.Column(db.Float, default=0)
    duration = db.Column(db.Interval, nullable=True)

    def __init__(self, miDistance=1, duration=timedelta()):
        # Note: __distance is stored internally in miles
        self.distance = miDistance
        self.duration = duration

    def getDistanceKM(self):
        return self.distance / MILES_IN_KM

    def setDistanceKM(self, distance):
        self.distance = distance * MILES_IN_KM

    def to_dict(self):
        outputDict = {"id": self.id,
                    "type": self.type,
                    "distance": self.distance,
                    "duration": str(self.duration)}

        return outputDict

