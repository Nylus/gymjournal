/**
 * class DataManager
 *
 * Manages downloading server data via AJAX for use in the front end
 * infrastructure.
 */
class DataManager
{
  constructor()
  {
    // Set up singleton instance
    if (! DataManager.instance)
    {
      this._store = {};
      this._ajaxHandler = ajaxUrl;
      this._clients = [];
      DataManager.instance = this;
    }

    return DataManager.instance;
  }

  // Accessor Methods
  get data()
  {
    return this._store;
  }

  // Public Methods

  /**
   * method retrieveData()
   *
   * Call GET to retrieve program data via AJAX
   */
  retrieveData()
  {
    var manager = this;
    $.ajax({
      type: "GET",
      url: manager._ajaxHandler,
      success: function (response)
      {
        manager._store.lastRetrieved = Date.now();
        manager._store.lastUpdated = Date.now();
        manager._store.data = response;

        manager._notifyClients();
      }
    });
  }

  updateSession(obj)
  {
    let day = obj.program_day;
    let index = day - 1;

    this._store.data.sessions[index] = obj;
    this._store.data.current_day = day + 1;

    this._store.lastUpdated = Date.now();
    this._notifyClients();
  }

  // Private Methods

  _notifyClients()
  {
    for (const callback of this._clients)
    {
      callback();
    }
  }

  /**
   * method registerCallback(function callback)
   *
   * Register a callback function for when data has been retrieved from server.
   */
  registerCallback(callback)
  {
    this._clients.push(callback);
  }
}

// Set up DataManager as a singleton
const dataManager = new DataManager();
// export default dataManager;



/**
 * class HalfMarathonController
 *
 * Controller for facilitating UI updates and interface changes for the
 * HalfMarathonProgram view.
 */
class HalfMarathonController
{
  constructor()
  {
    if (! HalfMarathonController.instance)
    {
      HalfMarathonController.instance = this;
      dataManager.registerCallback(this.makeCallback());

    }

    return HalfMarathonController.instance;

  }

  // Public methods
  start()
  {
    let _this = this;

    _this._visibleDay = 0;
    dataManager.retrieveData();

    $('#input-form').submit(function (e)
    {
      $.ajax({
        type: "POST",
        url: ajaxUrl,
        data: $('form').serialize(), // serializes the form's elements.
        success: _this.ajaxDidReturn
      });
      e.preventDefault(); // block the traditional submission of the form.
    });

    // Inject our CSRF token into our AJAX request.
    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrf_token);
        }
      }
    });
  }

  displayProgramData()
  {
    console.log("displayProgramData()");

    let data = dataManager.data.data;
    let day = data.current_day;

    this.loadDisplayForDay(day);
  }

  loadDisplayForDay(day)
  {
    let session = dataManager.data.data.sessions[day - 1];
    $("#day").val(day);
    $("#day-title").text("Day " + day);

    if (session.exercises[0].sets.length > 0)
    {
      if (session.exercises[0].sets[0].type == "running_set")
      {
        // Exercise consists of a running set
        let distance = session.exercises[0].sets[0].distance;
        $("#description-label").text("Distance: " + distance + " miles");
        $("label[for=time]").css("display", "inline");
        $("#time").css("display", "inline");
        $("#time").val(session.exercises[0].sets[0].duration);
      }
    }
    else
    {
      $("#description-label").text(session.exercises[0].description);
      $("label[for=time]").css("display", "none");
      $('#time').css("display", "none");

    }

    this._visibleDay = day;

    // Hide/Show "Previous" button
    if (this._visibleDay == 1)
    {
      $("#prevButton").css("display", "none");
    }
    else
    {
      $("#prevButton").css("display", "inline");
    }

    // Hide/Show "Next" button
    if (this._visibleDay == dataManager.data.data.sessions.length)
    {
      $("#nextButton").css("display", "none");
    }
    else
    {
      $("#nextButton").css("display", "inline");
    }
  }

  navigateToWeekAndDay(week, day)
  {
    this.loadDisplayForDay(((week - 1) * 7) + day);
  }

  previousButtonClicked()
  {
    this.loadDisplayForDay(this._visibleDay - 1);
  }

  nextButtonClicked()
  {
    this.loadDisplayForDay(this._visibleDay + 1);
  }

  // Callbacks
  makeCallback()
  {
    let obj = this;
    return function()
    {
      console.log("Finished Downloading");
      console.log(dataManager.data);

      obj.displayProgramData();
    };
  }

  ajaxDidReturn (response)
  {
    console.log(response);  // display the returned data in the console.
    var hasError = false;

    // Show Error msgs
    if ("error" in response &&
        response.error.time[0] === "Invalid input.")
    {
      hasError = true;
      $("#time-error").css("display", "inline");
    }
    else
    {
      $("#time-error").css("display", "none");
    }

    // Show Saved msg
    if (hasError)
    {
      $("#form-success").css("display", "none");
    }
    else
    {
      $("#form-success").css("display", "inline");
      dataManager.updateSession(response);
    }
  }


}

// Set up halfMarathonController as a singleton
const halfMarathonController = new HalfMarathonController();
//export default halfMarathonController;



$(document).ready(function()
{
  // Start the View Controller
  halfMarathonController.start();
});
