from gymJournal import db
from gymJournal.models import Session

class DynamicSession(Session):
    __tablename__ = 'dynamic_session'
    __mapper_args__ = {
        'polymorphic_identity':'dynamic_session',
    }

    percentOfMax = db.Column(db.Float, default=1.0)

    def to_dict(self):
        outputDict = Parent.to_dict(self)
        outputDict["percentOfMax"] = self.percentOfMax

        return outputDict

