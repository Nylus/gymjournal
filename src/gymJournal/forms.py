from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, IntegerField, DateTimeField, HiddenField, SelectField, RadioField, FieldList
from wtforms.validators import ValidationError, DataRequired, Regexp, Email, EqualTo
from wtforms.widgets.html5 import TimeInput

from gymJournal.models import User
from gymJournal.programs.pullupChallenge import WeightChoices, GoalChoices

class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please user a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please user a different email address.')

class DropForm(FlaskForm):
    drop = SubmitField(label='Drop Program')

class HalfMarathonForm(FlaskForm):
    timeValidator = Regexp(r'^([0-9]?[0-9]:)?([0-9]?[0-9]:)?[0-9][0-9]$')
    time = StringField('Duration', validators=[DataRequired(),
                                               timeValidator],
                                   render_kw={"placeholder": "HH:MM:SS"})
    day = HiddenField(validators=[DataRequired()])

class PullupChallengeRegistrationForm(FlaskForm):
    challengeGoal = RadioField('Challenge Goal', choices=[(member.value, name.capitalize()) for name, member in GoalChoices.__members__.items()], validators=[DataRequired()])
    weightCategory = SelectField('Weight', choices=[(member.value, name.capitalize()) for name, member in WeightChoices.__members__.items()])

class PullupChallengeForm(FlaskForm):
    pullupReps = FieldList(IntegerField('Reps', validators=[DataRequired()]), min_entries=1,
                                                                              max_entries=100)
    complete = SubmitField(label='Finish')
    save = SubmitField(label='Save')

class ConjugateMethodForm(FlaskForm):
    day = HiddenField(validators=[DataRequired()])

