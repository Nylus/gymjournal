"""Make sets a base class and refactor exercises and sessions

Revision ID: 846c23a9960f
Revises: 184670398087
Create Date: 2020-10-26 00:09:44.163412

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '846c23a9960f'
down_revision = '184670398087'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('set',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('type', sa.String(length=64), nullable=True),
    sa.Column('exercise_id', sa.Integer(), nullable=True),
    sa.Column('distance', sa.Float(), nullable=True),
    sa.Column('duration', sa.Interval(), nullable=True),
    sa.ForeignKeyConstraint(['exercise_id'], ['exercise.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.drop_table('sets')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('sets',
    sa.Column('id', sa.INTEGER(), nullable=False),
    sa.Column('exercise_id', sa.INTEGER(), nullable=True),
    sa.Column('reps', sa.INTEGER(), nullable=True),
    sa.Column('weight', sa.FLOAT(), nullable=True),
    sa.Column('duration', sa.DATETIME(), nullable=True),
    sa.ForeignKeyConstraint(['exercise_id'], ['exercise.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.drop_table('set')
    # ### end Alembic commands ###
