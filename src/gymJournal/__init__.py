from datetime import timedelta

from flask import Flask, render_template, flash, redirect, url_for, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager, current_user, login_user, logout_user, login_required
from .config import Config

from werkzeug.urls import url_parse

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'login'

from gymJournal import models, errors
from gymJournal.models import User, Program
from gymJournal.forms import (ConjugateMethodForm,
                              DropForm,
                              HalfMarathonForm,
                              LoginForm,
                              PullupChallengeForm,
                              PullupChallengeRegistrationForm,
                              RegistrationForm,
                              )
from gymJournal.programs.halfMarathon import HalfMarathon
from gymJournal.programs.pullupChallenge import PullupChallenge
from gymJournal.programs.conjugateMethod import ConjugateMethod



#Automatically import libraries for debug
@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User}

#Home page servlet
@app.route('/')
@login_required
def index():
    current_programs = Program.query.filter_by(user_id=current_user.id, completeDate=None)
    past_programs = Program.query.filter_by(user_id=current_user.id).filter(Program.completeDate != None)

    return render_template('dashboard.html', current_programs=current_programs,
                                             past_programs=past_programs)

#####################################################################
####################### Program Management ##########################
#####################################################################

#Programs page servlet
@app.route('/programs')
def programs():
    return render_template('programs.html')

#Half marathon preview servlet
@app.route('/programs/halfMarathon', methods=['GET', 'POST'])
@login_required
def halfMarathonPreview():
    if request.method == 'POST':
        if request.form['add_button'] == 'halfMarathon':
            newProgram = HalfMarathon(current_user)
            db.session.add(newProgram)
            db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('halfMarathonPreview.html', calendar=HalfMarathon(current_user).generatePreview(),
                                                           preview=True)

# Pullup Challenge preview servlet
@app.route('/programs/pullupChallenge', methods=['GET', 'POST'])
@login_required
def pullupChallengePreview():
    if request.method == 'POST':
        if request.form['add_button'] == 'pullupChallenge':
            form = PullupChallengeRegistrationForm()
            if form.validate_on_submit():
                newProgram = PullupChallenge(current_user)
                newProgram.challengeGoal = int(form.challengeGoal.data)
                newProgram.assistance = form.weightCategory.data
                db.session.add(newProgram)
                db.session.commit()
                 
                response = redirect(url_for('index'))
            else:
                response = jsonify(error=form.errors)
        return redirect(url_for('index'))
    else:
        return render_template('pullupChallengePreview.html', preview=True,
                                                              form=PullupChallengeRegistrationForm())

# Conjugate Method preview servlet
@app.route('/programs/conjugateMethod', methods=['GET', 'POST'])
@login_required
def conjugateMethodPreview():
    if request.method == 'POST':
        if request.form['add_button'] == 'conjugateMethod':
            newProgram = ConjugateMethod(current_user)
            db.session.add(newProgram)
            db.session.commit()
        return redirect(url_for('index'))

    else:
        return render_template('conjugateMethodPreview.html', calendar=ConjugateMethod(current_user).generatePreview(),
                                                              preview=True)

#####################################################################
####################### Data Entry ##################################
#####################################################################

# TODO: Save a last updated time so that new edits will not be saved over
#   older edits that happen from another source. May also need to look
#   into sockets and persistent connections

@app.route('/programdata/<string:program_type>/<int:program_id>', methods=['GET', 'POST'])
@login_required
def programData(program_type, program_id):
    response = None
    myProgram = Program.query.get(program_id)

    programTypesMap = {"halfMarathon"   : {"class": HalfMarathon,
                                           "form": HalfMarathonForm},
                       "pullupChallenge": {"class": PullupChallenge,
                                           "form": PullupChallengeForm},
                       "conjugateMethod": {"class": ConjugateMethod,
                                           "form": ConjugateMethodForm}}

    if program_type in programTypesMap:
        # Instantiate the correct program class
        myProgramClass = programTypesMap[program_type]["class"]
        myProgram = myProgramClass.query.get(program_id)

        if request.method == 'POST':
            # POST request - process form submission

            form = programTypesMap[program_type]["form"]()
            if form.validate_on_submit():
                response = myProgram.processFormData(form)
            else:
                response = jsonify(error=form.errors)

        elif request.method == 'GET':
            # GET request - return data structure
            response = myProgram.to_dict()

    return response
    

#@app.route('/programs/halfMarathon/<int:program_id>', methods=['GET', 'POST'])
@app.route('/programs/halfMarathon/<int:program_id>', methods=['GET', 'POST'])
@login_required
def myHalfMarathon(program_id):
    myProgram = Program.query.get(program_id)

    # Default to current day in program
    # TODO: Can enhance this by storing date of last update in database,
    #   check if the date is different from the last day info was saved.
    day = 1

    if request.method == 'POST':
        # Drop program if the drop form was submitted
        if DropForm(request.form).validate():
            db.session.delete(myProgram)
            db.session.commit()
            return redirect(url_for('index'))

    return render_template('halfMarathonPreview.html', calendar=myProgram.generatePreview(),
                                                        preview=False,
                                                           form=HalfMarathonForm(),
                                                      drop_form=DropForm(),
                                                        program=myProgram,
                                                            day=day)

@app.route('/programs/pullupChallenge/<int:program_id>', methods=['GET', 'POST'])
@login_required
def myPullupChallenge(program_id):
    myProgram = Program.query.get(program_id)

    day = 1
    if request.method == 'POST':
        # Drop program if the drop form was submitted
        if DropForm(request.form).validate():
            db.session.delete(myProgram)
            db.session.commit()
            return redirect(url_for('index'))

    return render_template('pullupChallengePreview.html', preview=False,
                                                           form=PullupChallengeForm(),
                                                      drop_form=DropForm(),
                                                        program=myProgram,
                                                            day=day)

@app.route('/programs/conjugateMethod/<int:program_id>', methods=['GET', 'POST'])
@login_required
def myConjugateMethod(program_id):
    myProgram = Program.query.get(program_id)

    day = 1
    if request.method == 'POST':
        # Drop program if the drop form was submitted
        if DropForm(request.form).validate():
            db.session.delete(myProgram)
            db.session.commit()
            return redirect(url_for('index'))

    return render_template('conjugateMethodPreview.html', preview=False,
                                                           form=ConjugateMethodForm(),
                                                      drop_form=DropForm(),
                                                        program=myProgram,
                                                            day=day)

#####################################################################
############## Authentication + Registration ########################
#####################################################################

#Login page servlet
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)

#Logout page servlet
@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))

#Register page servlet
@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)
