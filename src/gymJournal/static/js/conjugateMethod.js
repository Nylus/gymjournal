/**
 * class DataManager
 *
 * Manages downloading server data via AJAX for use in the front end
 * infrastructure.
 */
class DataManager
{
  constructor()
  {
    // Set up singleton instance
    if (! DataManager.instance)
    {
      this._store = {};
      this._ajaxHandler = ajaxUrl;
      this._clients = [];
      DataManager.instance = this;
    }

    return DataManager.instance;
  }

  // Accessor Methods
  get data()
  {
    return this._store;
  }

  // Public Methods

  /**
   * method retrieveData()
   *
   * Call GET to retrieve program data via AJAX
   */
  retrieveData()
  {
    var manager = this;
    $.ajax({
      type: "GET",
      url: manager._ajaxHandler,
      success: function (response)
      {
        manager._store.lastRetrieved = Date.now();
        manager._store.lastUpdated = Date.now();
        manager._store.data = response;

        manager._notifyClients();
      }
    });
  }

  updateSession(obj)
  {
    let day = obj.program_day;
    let index = day - 1;

    this._store.data.sessions[index] = obj;
    this._store.data.current_day = day + 1;

    this._store.lastUpdated = Date.now();
    this._notifyClients();
  }

  // Private Methods

  _notifyClients()
  {
    for (const callback of this._clients)
    {
      callback();
    }
  }

  /**
   * method registerCallback(function callback)
   *
   * Register a callback function for when data has been retrieved from server.
   */
  registerCallback(callback)
  {
    this._clients.push(callback);
  }
}

// Set up DataManager as a singleton
const dataManager = new DataManager();
// export default dataManager;



/**
 * class ConjugateMethodController
 *
 * Controller for facilitating UI updates and interface changes for the
 * ConjugateMethod view.
 */
class ConjugateMethodController
{
  constructor()
  {
    if (! ConjugateMethodController.instance)
    {
      ConjugateMethodController.instance = this;
      dataManager.registerCallback(this.makeCallback());

    }

    return ConjugateMethodController.instance;

  }

  // Accessors
  
  get sessions()
  {
    return dataManager.data.data.sessions;
  }

  // Public methods
  start()
  {
    let _this = this;

    //_this._visibleSets = 0;
    dataManager.retrieveData();

    $('#input-form').submit(function (e)
    {
      $.ajax({
        type: "POST",
        url: ajaxUrl,
        data: $('form').serialize(), // serializes the form's elements.
        success: _this.ajaxDidReturn
      });
      e.preventDefault(); // block the traditional submission of the form.
    });

    // Inject our CSRF token into our AJAX request.
    $.ajaxSetup({
      beforeSend: function(xhr, settings) {
        if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
          xhr.setRequestHeader("X-CSRFToken", csrf_token);
        }
      }
    });
  }

  renderNewFormTemplate()
  {
    // If templates are supported by this browser
    if ('content' in document.createElement('template'))
    {
      //this._visibleSets++;

      //let setNumber = this._visibleSets;
      //let setIndex = setNumber - 1;
      //let idSuffix = "-" + setIndex;
      //var tdiv = document.getElementById('form-container');
      //var template = document.getElementById('pullup-form');

      //var clone = template.content.cloneNode(true);

      //var enclosingTag = clone.querySelector("p");
      //
      //enclosingTag.id += idSuffix;

      //var error = clone.querySelector("#reps-error");
      //error.id += idSuffix;

      //var input = clone.querySelector("#pullupReps");
      //input.id += idSuffix;
      //input.name += idSuffix;

      //var label = clone.querySelector("#pullupsRepsLabel");
      //label.id += idSuffix;
      //label.htmlFor = input.name;
      //label.innerHTML = "Set #" + setNumber + " Reps:";

      //// Add the cloned template to the existing HTML doc
      //tdiv.appendChild(clone);
    }
    else
    {
      // Templates not supported - show an error msg or something
    }
  }

  //updateSet(set)
  //{
  //  if (set.index >= this._visibleSets)
  //  {
  //    this.renderNewFormTemplate();
  //  }

  //  // Set the value on the input
  //  let identifier = "pullupReps-" + set.index;
  //  var input = document.getElementById(identifier);
  //  input.value = set.reps;
  //}

  displayProgramData()
  {
    console.log("displayProgramData()");

    let data = dataManager.data.data;
    let day = data.current_day;

    this.loadDisplayForDay(day);
  }

  loadDisplayForDay(day)
  {
    let session = this.sessions[day - 1];
    $("#day").val(day);
    $("#day-title").text("Day " + day);

    // Clear the form container
    var formContainer = $("#form-container");
    formContainer.empty();

    for (var exercise in session.exercises)
    {
      if (exercise.sets.length > 0)
      {
        jk
        if (exercise.sets[0].type == "running_set")
        {
          // Exercise consists of a running set
          let distance = exercise.sets[0].distance;
          $("#description-label").text("Distance: " + distance + " miles");
          $("label[for=time]").css("display", "inline");
          $("#time").css("display", "inline");
          $("#time").val(exercise.sets[0].duration);
        }
        else if (exercise.sets[0].type == "weight_set")
        {
          $("label[for=time]").css("display", "inline");
          $("#time").css("display", "inline");
          $("#time").val(exercise.sets[0].duration);
          
        }
      }
      else
      {
        $("#description-label").text(exercise.description);
        $("label[for=time]").css("display", "none");
        $('#time').css("display", "none");

      }
    }


    this._visibleDay = day;

    // Hide/Show "Previous" button
    if (this._visibleDay == 1)
    {
      $("#prevButton").css("display", "none");
    }
    else
    {
      $("#prevButton").css("display", "inline");
    }

    // Hide/Show "Next" button
    if (this._visibleDay == dataManager.data.data.sessions.length - 1)
    {
      $("#nextButton").css("display", "none");
    }
    else
    {
      $("#nextButton").css("display", "inline");
    }
  }

  //previousButtonClicked()
  //{
  //  this.loadDisplayForDay(this._visibleDay - 1);
  //}

  //nextButtonClicked()
  //{
  //  this.loadDisplayForDay(this._visibleDay + 1);
  //}
  

  // Button Actions
  //addSetClicked()
  //{
  //  this.updateSet({index: this._visibleSets,
  //                  reps: 0});

  //  if (this._visibleSets > 1)
  //  {
  //    document.getElementById("removeSetButton").disabled = false;
  //  }
  //}

  //removeSetClicked()
  //{
  //  // Can't be fewer than 1 set displayed at any time
  //  if (this._visibleSets > 1)
  //  {
  //    let setIndex = this._visibleSets - 1;
  //    var enclosingTag = document.getElementById("pullupEntry-" + setIndex);

  //    // Delete last set in the list
  //    enclosingTag.remove();

  //    this._visibleSets--;

  //    if (this._visibleSets <= 1)
  //    {
  //      document.getElementById("removeSetButton").disabled = true;
  //    }
  //  }
  //}

  // Callbacks
  makeCallback()
  {
    let obj = this;
    return function()
    {
      console.log("Finished Downloading");
      console.log(dataManager.data);

      obj.displayProgramData();

      //for (var set in obj.setsFromDataManager)
      //{
      //  obj.updateSet(obj.setsFromDataManager[set]);
      //}
    };
  }

  ajaxDidReturn (response)
  {
    console.log(response);  // display the returned data in the console.
    var hasError = false;

    // Show Error msgs
    //if ("error" in response &&
    //    response.error.time[0] === "Invalid input.")
    //{
    //  hasError = true;
    //  $("#time-error").css("display", "inline");
    //}
    //else
    //{
    //  $("#time-error").css("display", "none");
    //}

    // Show Saved msg
    if (hasError)
    {
      $("#form-success").css("display", "none");
    }
    else
    {
      $("#form-success").css("display", "inline");
      dataManager.updateSession(response);
    }
  }


}

// Set up pullupChallengeController as a singleton
const conjugateMethodController = new ConjugateMethodController();
//export default pullupChallengeController;



$(document).ready(function()
{
  // Start the View Controller
  conjugateMethodController.start();
});


